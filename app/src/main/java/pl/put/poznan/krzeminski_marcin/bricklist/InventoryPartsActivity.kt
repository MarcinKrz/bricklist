package pl.put.poznan.krzeminski_marcin.bricklist

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SwitchCompat
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.CompoundButton
import android.widget.Switch
import kotlinx.android.synthetic.main.activity_inventory_parts.*
import kotlinx.android.synthetic.main.content_inventory_parts.*

class InventoryPartsActivity : AppCompatActivity() {
    var adapter: PartAdapter? = null
    var partsList = ArrayList<PartDto>()
    var id: Int = 1
    var active: Int = 1
    var name = "Items"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inventory_parts)
        setSupportActionBar(toolbar)
        val dbHandler = Database(this, null, null, 1)
//        dbHandler.createDataBase()
        id = (intent.extras ?: return).getInt("id")
        active = (intent.extras ?: return).getInt("active")
        name  = (intent.extras ?: return).getString("name")
        Log.e("id inventory to ", id.toString())

        val partsDao = dbHandler.findAllInventoryParts(id)
        Log.e("myApp partsDao.len", partsDao.size.toString())
        for (part in partsDao) {
            val element = dbHandler.createPartDto(part)
            partsList.add(element)
            Log.e("myApp part:", element.namePart + " " + element.nameColor)
        }
        adapter = PartAdapter(partsList, this)
        partsListView.adapter = adapter
        dbHandler.close()

        title=name

    }
    override fun finish() {
        val data = Intent()
        setResult(Activity.RESULT_OK,data)
        super.finish()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_items, menu)

        val item = menu!!.findItem(R.id.archivedOptionMenu)
        item.setActionView(R.layout.switch_item)

        val mySwitch = item.actionView.findViewById<Switch>(R.id.archiveId)

        if(active ==1){
            mySwitch.setChecked(true)
        }
        else{
            mySwitch.setChecked(false)
        }

        mySwitch.setOnCheckedChangeListener { buttonView, isChecked ->
                Log.e("zdarzenie!:",": "+isChecked)
                saveChangeActive(isChecked)

        }
        return true
    }

    fun saveChangeActive(activeState: Boolean){
        var active = 1
        if(activeState==false){
            active=0
        }
        val dbHandler = Database(this, null, null, 1)
        dbHandler.updateStateInventory(id,active)
        dbHandler.close()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {
            R.id.exportOptionMenu -> {
                val intent = Intent(this, ExportActivity::class.java)
                    .putExtra("id", id)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun refreshPartsListView() {
        partsList.clear()
        val dbHandler = Database(this, null, null, 1)
        val partsDao = dbHandler.findAllInventoryParts(id)
        for (part in partsDao) {
            val element = dbHandler.createPartDto(part)
            partsList.add(element)
        }
        dbHandler.close()
        adapter?.notifyDataSetChanged()
    }




}
