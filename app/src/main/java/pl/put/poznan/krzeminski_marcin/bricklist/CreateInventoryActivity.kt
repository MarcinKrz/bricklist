package pl.put.poznan.krzeminski_marcin.bricklist

import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_create_inventory.*
import kotlinx.android.synthetic.main.content_create_inventory.*
import org.jetbrains.anko.doAsync
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import java.io.BufferedInputStream
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.net.URL
import javax.xml.parsers.DocumentBuilderFactory

class CreateInventoryActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_inventory)
        setSupportActionBar(toolbar)
    }

    override fun finish() {
        val data = Intent()
        setResult(Activity.RESULT_OK, data)
        super.finish()
    }

    fun downloadInventory(view: View) {
        doAsync {
            try {
                Log.e("!!!!!pobieranie invento", "1")
                val url = URL(MyApplication.prefixUrl + urlText.text.toString() + MyApplication.postfixUrl)
                val connection = url.openConnection()
                connection.connect()
//            val lengthOfFile = connection.contentLength
                //            var total: Long = 0
//            var progress = 0
                val isStream = url.openStream()

                val testDirectory = File("$filesDir/XML")
                if (!testDirectory.exists()) testDirectory.mkdir()
                val fos = FileOutputStream("$testDirectory/newInventory.xml")
                val data = ByteArray(1024)
                var count = 0
                count = isStream.read(data)
                while (count != -1) {
//                total += count.toLong()
//                val progress_temp = total.toInt() * 100 / lengthOfFile
//                if (progress_temp % 10 == 0 && progress != progress_temp) {
//                    progress = progress_temp
//                }
                    fos.write(data, 0, count)
                    count = isStream.read(data)
                }
                isStream.close()
                fos.close()
                val resultItems = readFileItems()
                val rowId = saveInventory(nameProjectText.text.toString())
                saveItemsDao(rowId, resultItems)
            } catch (e: Throwable) {
                runOnUiThread {
                    Toast.makeText(
                        view.context,
                        "Nie udało się pobrać wybranego modelu",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    fun saveInventory(name: String): Long {
        val newInventory = ContentValues()
        newInventory.put("Name", name)
        newInventory.put("Active", 1)
        val timeNow = System.currentTimeMillis() / 1000

        val dbHandler = Database(this, null, null, 1)
//        dbHandler.createDataBase()
        newInventory.put("LastAccessed", timeNow)
        val id = dbHandler.insertInventory(newInventory)
        dbHandler.close()
        return id
    }

    fun saveItemsDao(inventoryId: Long, items: MutableList<ItemContainer>) {
        val dbHandler = Database(this, null, null, 1)
//        dbHandler.createDataBase()
        for (item: ItemContainer in items) {
            val newItem = ContentValues()
            newItem.put("InventoryID", inventoryId)
            newItem.put("TypeID", dbHandler.findItemTypeId(item.itemType))
//            newItem.put("ItemID", item.itemID)
            val itemID = dbHandler.findItemIDFromParts(item.itemId)
            if (itemID == 0) {
                continue
            }
            newItem.put("ItemID", itemID)
            newItem.put("QuantityInSet", item.qty)
            newItem.put("QuantityInStore", 0)
            val colorId = dbHandler.findColorId(item.color)
            newItem.put("ColorID", colorId)
            newItem.put("Extra", item.extra)
            val rowId = dbHandler.insertItem(newItem)
            val code = dbHandler.findCodeToImage(colorId, itemID)
//            if (code != 0) {
//            if(dbHandler.checkImageExists(colorId,itemID)==false){
            DownloadImage(item.itemId, item.color, code, colorId, itemID)
//            Log.e("a", "a")}
//            else{
//                Log.e("Jest wiec nie pobieram"," a")
//            }
//            }
        }
        dbHandler.close()
        runOnUiThread { Toast.makeText(this, "Egzemplarz został pomyslnie pobrany", Toast.LENGTH_SHORT).show() }
    }

    fun readFileItems(): MutableList<ItemContainer> {
        var itemsList: MutableList<ItemContainer> = mutableListOf()
        val filename = "newInventory.xml"
        val path = filesDir
        val inDir = File(path, "XML")

        if (inDir.exists()) {
            val file = File(inDir, filename)
            if (file.exists()) {
                val xmlDoc: Document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file)
                xmlDoc.documentElement.normalize()

                val items: NodeList = xmlDoc.getElementsByTagName("ITEM")
                for (i in 0..items.length - 1) {
                    val itemNode: Node = items.item(i)
                    if (itemNode.getNodeType() == Node.ELEMENT_NODE) {
                        val elem = itemNode as Element
                        val children = elem.childNodes

                        var newItem = ItemContainer()

                        for (j in 0..children.length - 1) {
                            val node = children.item(j)
                            if (node is Element) {
                                when (node.nodeName) {
                                    "ITEMTYPE" -> newItem.itemType = node.textContent
                                    "ITEMID" -> newItem.itemId = node.textContent
                                    "QTY" -> newItem.qty = node.textContent.toInt()
                                    "COLOR" -> newItem.color = node.textContent.toInt()
                                    "EXTRA" -> newItem.extra = node.textContent
                                    "ALTERNATE" -> newItem.alternate = node.textContent
                                }
                            }
                        }
                        if (newItem.alternate == "N") {
                            itemsList.add(newItem)
                        }
//                        Log.e("@@@@Read Item:",":"+newItem.itemType+" "+newItem.itemId+" "+newItem.qty+" "+newItem.color+" "+newItem.extra)

                    }
                }
//                val dbHandler = Database(this, null, null, 1)
////                dbHandler.checkDataBase()
//                dbHandler.createDataBase()
//                dbHandler.insertInventory(nameInventoryText.text.toString())
//                dbHandler.selectAll("a")
            }
        }
        Log.e("Wielkosc Inventory", itemsList.size.toString())
        return itemsList
    }

    fun saveDownloadImage1(code: Int, blobValues: ContentValues): Int {
        val dbHandler = Database(this, null, null, 1)
        val rowId = dbHandler.insertImage(code, blobValues)
        dbHandler.close()
        return rowId
    }

    fun saveDownloadImage2(blobValues: ContentValues) {
        val dbHandler = Database(this, null, null, 1)
        val rowId = dbHandler.saveImageWithoutCode(blobValues)
        dbHandler.close()
    }

    fun DownloadImage(
        codeToImage2: String,
        colorCode: Int,
        code: Int,
        colorId: Int,
        itemID: Int
    ) {
        doAsync {
            try {

                val url = "https://www.lego.com/service/bricks/5/2/$code"

                BufferedInputStream(URL(url).content as InputStream).use {
                    val baf = ArrayList<Byte>()
                    var current: Int
                    while (true) {
                        current = it.read()
                        if (current == -1)
                            break
                        baf.add(current.toByte())
                    }
                    val blob = baf.toByteArray()
                    val blobValues = ContentValues()
                    blobValues.put("Image", blob)
                    Log.e("!!!!BLOB ", blob.size.toString())
                    if (saveDownloadImage1(code, blobValues) == 0) {
                        blobValues.put("ColorID", colorId)
                        blobValues.put("ItemID", itemID)
                        saveDownloadImage2(blobValues)
                    }
                    Log.e(
                        "Pobralem w 1: ",
                        "" + url + " " + codeToImage2 + " " + colorCode + " " + code + " " + colorId + " " + itemID
                    )
                }
            } catch (e: Throwable) {
                try {
                    e.printStackTrace()
                    val url = "http://img.bricklink.com/P/" + colorCode + "/" + codeToImage2 + ".gif"
                    BufferedInputStream(URL(url).content as InputStream).use {
                        val baf = ArrayList<Byte>()
                        var current: Int
                        while (true) {
                            current = it.read()
                            if (current == -1)
                                break
                            baf.add(current.toByte())
                        }
                        val blob = baf.toByteArray()
                        val blobValues = ContentValues()
                        blobValues.put("Image", blob)
                        if (saveDownloadImage1(code, blobValues) == 0) {
                            blobValues.put("ColorID", colorId)
                            blobValues.put("ItemID", itemID)
                            saveDownloadImage2(blobValues)
                        }
                        Log.e(
                            "Download nr  2: ",
                            "" + url + " " + codeToImage2 + " " + colorCode + " " + code + " " + colorId + " " + itemID
                        )

                    }

                } catch (e: Throwable) {

                    try {
                        val url = "https://www.bricklink.com/PL/" + codeToImage2 + ".jpg"
                        BufferedInputStream(URL(url).content as InputStream).use {
                            val baf = ArrayList<Byte>()
                            var current: Int
                            while (true) {
                                current = it.read()
                                if (current == -1)
                                    break
                                baf.add(current.toByte())
                            }
                            val blob = baf.toByteArray()
                            val blobValues = ContentValues()
                            blobValues.put("Image", blob)
                            blobValues.put("ItemID", itemID)
                            blobValues.put("ColorID", colorId)
                            saveDownloadImage2(blobValues)
                            Log.e(
                                "zapisalem nr 3: ",
                                url + " " + codeToImage2 + " " + colorCode + " " + code + " " + colorId + " " + itemID
                            )
//                            Log.e("zapisalem zdj z 3 linku", ":" + itemID + "+ " + colorId)
                        }
                    } catch (e: Throwable) {
                        e.printStackTrace()
                        Log.e(":( :( :( @@@@@@@@@", "Nie znalazłem pod zadnym likniem zdjecia")
                    }
                }
            }

        }
    }

    fun exportToFile(view: View) {
//        doAsync {
//
//            val isStream = url.openStream()
//            val testDirectory = File("$filesDir/XML")
//            if (!testDirectory.exists()) testDirectory.mkdir()
//            val fos = FileOutputStream("$testDirectory/newInventory.xml")
//            val data = ByteArray(1024)
//            var count = 0
//            var total: Long = 0
//            var progress = 0
//            count = isStream.read(data)
//            while (count != -1) {
////                total += count.toLong()
////                val progress_temp = total.toInt() * 100 / lengthOfFile
////                if (progress_temp % 10 == 0 && progress != progress_temp) {
////                    progress = progress_temp
////                }
//                Log.e("myApp data", data.toString())
//                fos.write(data, 0, count)
//                count = isStream.read(data)
//            }
//            isStream.close()
//            fos.close()
//        }
    }

}
