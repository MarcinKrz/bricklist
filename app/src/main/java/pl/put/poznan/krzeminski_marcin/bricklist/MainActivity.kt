package pl.put.poznan.krzeminski_marcin.bricklist

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Switch
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val REQUEST_CODE = 200
    var allInvetStatus = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        getSupportActionBar()!!.hide()
        initiationActivity(allInvetStatus)

    }


    fun initiationActivity(all: Boolean) {


        val dbHandler = Database(this, null, null, 1)
        dbHandler.createDataBase()

        var inventoriesList: ArrayList<InventoryDao>
        if (all) {
            inventoriesList = dbHandler.findAllInventories()
        } else {
            inventoriesList = dbHandler.findAllActiveInventories()
        }
        dbHandler.close()
//        var adapter = ArrayAdapter<InventoryDao>(this, R.layout.inventory,inventoriesList)
//        inventoryListView.adapter = adapter
        var adapter = InventoryAdapter(this, inventoriesList)
        inventoryListView.adapter = adapter
//        inventoryListView.onItemClickListener = AdapterView.

    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        return super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu_main, menu)

        val item = menu!!.findItem(R.id.showALLID)
        item.setActionView(R.layout.switch_show_all)

        val mySwitch = item.actionView.findViewById<Switch>(R.id.switchShowId)

        mySwitch.setChecked(allInvetStatus)

        mySwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            Log.e("zdarzenie!:", ": " + isChecked)
            initiationActivity(isChecked)
            allInvetStatus = isChecked

        }
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
//        return super.onOptionsItemSelected(item)
        when (item?.itemId) {
            R.id.settingsMenu -> {
                val intent = Intent(this, SettingsActivity::class.java)
//                startActivity(intent)
                startActivityForResult(intent, REQUEST_CODE)

            }
            R.id.createInventory -> {
                val intent = Intent(this, CreateInventoryActivity::class.java)
//                startActivity(intent)
                startActivityForResult(intent, REQUEST_CODE)
            }
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if ((requestCode == REQUEST_CODE) && (resultCode == Activity.RESULT_OK)) {
            initiationActivity(allInvetStatus)
        }
    }


}
