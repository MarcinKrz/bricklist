package pl.put.poznan.krzeminski_marcin.bricklist

class ItemContainer {
    var itemType: String = ""
    var itemId: String = ""
    var qty: Int = 0
    var color: Int = 0
    var extra: String = ""
    var alternate: String = ""
}