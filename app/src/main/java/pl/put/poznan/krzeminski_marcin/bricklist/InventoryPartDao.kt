package pl.put.poznan.krzeminski_marcin.bricklist

class InventoryPartDao {
    var id: Int = 0
    var inventoryId: Int = 0
    var typeID: Int = 0
    var itemID: Int = 0
    var quantityInSet: Int = 0
    var quantityInStore: Int = 0
    var colorID: Int = 0
    var extra: Int = 0

    constructor(
        id: Int,
        inventoryId: Int,
        typeId: Int,
        itemId: Int,
        quantityInSet: Int,
        quantityInStore: Int,
        colorId: Int,
        extra: Int
    ) {
        this.id = id
        this.inventoryId = inventoryId
        this.typeID = typeId
        this.itemID = itemId
        this.quantityInSet = quantityInSet
        this.quantityInStore = quantityInStore
        this.colorID = colorId
        this.extra = extra
    }
}