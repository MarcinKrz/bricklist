package pl.put.poznan.krzeminski_marcin.bricklist

import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.View
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.content_settings.*

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        setSupportActionBar(toolbar)

        prefixUrlText.text = SpannableStringBuilder(MyApplication.prefixUrl)
        postfixUrlText.text = SpannableStringBuilder(MyApplication.postfixUrl)
        title = "Settings"
    }

    fun saveSettings(view: View) {
        MyApplication.prefixUrl= prefixUrlText.text.toString()
        Log.e("aaa",prefixUrlText.text.toString())
        MyApplication.postfixUrl = postfixUrlText.text.toString()
        Toast.makeText(this,"Zmiany zostały zapisane", Toast.LENGTH_SHORT).show()
    }

}
