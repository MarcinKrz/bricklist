package pl.put.poznan.krzeminski_marcin.bricklist

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v4.app.ActivityCompat.startActivityForResult
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

class InventoryAdapter : BaseAdapter {
    var invetoriesList = ArrayList<InventoryDao>()
    var context: Context? = null
    val REQUEST_CODE = 200
    var idInventory=0

    constructor(context: Context?, invetoriesList: ArrayList<InventoryDao>) : super() {
        this.invetoriesList = invetoriesList
        this.context = context

    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val view: View?
        val viewHolder: ViewHolder

        if (convertView == null) {
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(R.layout.inventory, parent, false)
            viewHolder = ViewHolder(view)
            view.tag = viewHolder
            Log.i("JSA", "set Tag for ViewHolder, position: " + position)
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }



        viewHolder.nameInventoryTextView.text = invetoriesList[position].name
        var selectedItem = getItem(position)



        viewHolder.nameInventoryTextView.setOnClickListener {
            val dbHandler = Database(context!!, null, null, 1)
            val timeNow = (System.currentTimeMillis() / 1000).toInt()
            dbHandler.updateLastAccessed(selectedItem.id,timeNow)
            dbHandler.close()
            Log.e("update czasu",""+timeNow)
            //            Toast.makeText(context, selectedItem.name, Toast.LENGTH_SHORT).show()
            val intent = Intent(context, InventoryPartsActivity::class.java)
                .putExtra("id", selectedItem.id)
                .putExtra("active",selectedItem.active)
                .putExtra("name",selectedItem.name)
            val activity =context as Activity
            activity.startActivityForResult(intent,REQUEST_CODE)
        }

        return view
    }


    override fun getItem(position: Int): InventoryDao {
        return invetoriesList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return invetoriesList.size
    }

    private class ViewHolder(view: View?) {
        val nameInventoryTextView: TextView

        init {
            this.nameInventoryTextView = view?.findViewById(R.id.nameProjectText) as TextView
        }
    }
}



