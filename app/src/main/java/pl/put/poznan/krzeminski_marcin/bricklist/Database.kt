package pl.put.poznan.krzeminski_marcin.bricklist

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import java.io.FileOutputStream
import java.io.IOException

class Database(context: Context, name: String?, factory: SQLiteDatabase.CursorFactory?, version: Int) :
    SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {

    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "BrickList.db"
        val TABLE_INVENTORIES = "Inventories"
        val TABLE_INVENTORIES_PARTS = "InventoriesParts"
    }

    var myContext: Context

    init {
        myContext = context
    }

    //The Android's default system path of your application database.
    private val DB_PATH = "/data/data/" + myContext.packageName + "/databases/"
    private val DB_NAME = "BrickDatabase.db"


    override fun onCreate(db: SQLiteDatabase?) {
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
    }

    fun saveImageWithoutCode(value:ContentValues):Long{
        val db = this.writableDatabase
        val rowId = db.insert("Codes", null, value)
        db.close()
        return rowId
    }

    fun updateQuantity(id: Int, quantity: Int) {
        val db = this.writableDatabase
        var values = ContentValues()
        values.put("QuantityInStore", quantity)
        val retVal = db.update(TABLE_INVENTORIES_PARTS, values, "id=$id", null)
        if (retVal >= 1) {
            Log.v("@@@WWe", " Record updated")
        } else {
            Log.v("@@@WWe", " Not updated")
        }
        db.close()
    }

    fun updateLastAccessed(id:Int, time:Int){
        val db = this.writableDatabase
        var values = ContentValues()
        values.put("LastAccessed", time)
        val retVal = db.update(TABLE_INVENTORIES, values, "id=$id", null)
        if (retVal >= 1) {
            Log.v("@@@WWe", " Record updated")
        } else {
            Log.v("@@@WWe", " Not updated")
        }
        db.close()
    }

    fun insertInventory(newInventory: ContentValues): Long {
        val db = this.writableDatabase
        val rowId = db.insert(TABLE_INVENTORIES, null, newInventory)
        db.close()
        return rowId
    }

    fun insertItem(newItem: ContentValues): Long {
        val db = this.writableDatabase
        val rowId = db.insert(TABLE_INVENTORIES_PARTS, null, newItem)
        db.close()
        return rowId
    }

    fun insertImage(code: Int, image: ContentValues) :Int{
        val db = this.writableDatabase
        val rowId = db.update("Codes", image, "Code=$code", null)
        db.close()
        return rowId
    }

    fun findImage(colorID: Int,itemID: Int): Bitmap? {
//        val query = "SELECT Image FROM Codes WHERE Code=$code"
        var query = "SELECT Image FROM Codes WHERE ColorID=$colorID and ItemID=$itemID"
        val db = this.readableDatabase
        val blob: ByteArray?
        val cursor = db.rawQuery(query, null)
        var image: Bitmap? = null
        if (cursor.moveToFirst()) {
            blob = cursor.getBlob(0)
            if (blob != null) {
                image = BitmapFactory.decodeByteArray(blob, 0, blob.size)
            }
        }
        if (cursor != null && !cursor.isClosed) {
            cursor.close()
        }
        return image
    }

    fun checkImageExists(colorID: Int,itemID: Int): Boolean? {
//        val query = "SELECT Image FROM Codes WHERE Code=$code"
       var ret  = false
        var query = "SELECT Image FROM Codes WHERE ColorID=$colorID and ItemID=$itemID"
        val db = this.readableDatabase
        val blob: ByteArray?
        val cursor = db.rawQuery(query, null)
        var image: Bitmap? = null
        if (cursor.moveToFirst()) {
          ret = true
        }
       return ret
    }

    fun findColorCodeFromColors(id:Int): Int {
        val query = "SELECT Code FROM Colors WHERE id=\"$id\""
        val db = this.readableDatabase
        val cursor = db.rawQuery(query, null)
        cursor.moveToNext()
        val id = Integer.parseInt(cursor.getString(0))
        cursor.close()
        db.close()
        return id
    }

    fun findColorId(colorCode: Int): Int {
        val query = "SELECT Id FROM Colors WHERE Code=\"$colorCode\""
        val db = this.readableDatabase
        val cursor = db.rawQuery(query, null)
        cursor.moveToNext()
        val id = Integer.parseInt(cursor.getString(0))
        cursor.close()
        db.close()
        return id
    }

    fun findItemTypeId(itemTypeCode: String): Int {
        val query = "SELECT Id FROM ItemTypes WHERE Code=\"$itemTypeCode\""
        val db = this.readableDatabase
        val cursor = db.rawQuery(query, null)
        cursor.moveToNext()
        val id = Integer.parseInt(cursor.getString(0))
        cursor.close()
        db.close()
        return id
    }

    fun findCodeFromItemType(itemTypeID: Int): String {
        val query = "SELECT Code FROM ItemTypes WHERE id=\"$itemTypeID\""
        val db = this.readableDatabase
        val cursor = db.rawQuery(query, null)
        cursor.moveToNext()
        val code = cursor.getString(0)
        cursor.close()
        db.close()
        return code
    }

    fun findCodeFromParts(id: Int): String {
        val query = "SELECT Code FROM Parts WHERE Id=\"$id\""
        val db = this.readableDatabase
        val cursor = db.rawQuery(query, null)
        var code = ""
        if (cursor.moveToNext()) {
            code = cursor.getString(0)
        }
        cursor.close()
        db.close()
        return code
    }

    fun updateStateInventory (id:Int, active:Int){
        val db = this.writableDatabase
        var values = ContentValues()
        values.put("Active",active)
        val retVal = db.update(TABLE_INVENTORIES, values, "id=$id", null)
        if (retVal >= 1) {
            Log.e("@@@WWe", " Record updated"+active)
        } else {
            Log.e("@@@WWe", " Not updated")
        }
        db.close()
    }

    fun findItemIDFromParts(code: String): Int {
        val query = "SELECT Id FROM Parts WHERE Code=\"$code\""
        val db = this.readableDatabase
        val cursor = db.rawQuery(query, null)
        var id = 0
        if (cursor.moveToNext()) {
            id = cursor.getInt(0)
        }
        cursor.close()
        db.close()
        return id
    }

    fun findInventoryById(id: String): InventoryDao {
        val query = "SELECT * FROM Inventory WHERE id=\"$id\""
        val db = this.readableDatabase
        val cursor = db.rawQuery(query, null)
        cursor.moveToNext()
        val inventory = InventoryDao(cursor.getInt(0), cursor.getString(1), cursor.getInt(2), cursor.getInt(3))
        cursor.close()
        db.close()
        return inventory
    }

    fun findAllInventoryParts(id: Int): ArrayList<InventoryPartDao> {
        val query = "SELECT * from  $TABLE_INVENTORIES_PARTS WHERE InventoryID=$id"
//        val query = "SELECT * from  $TABLE_INVENTORIES_PARTS"

        val db = this.readableDatabase
        val cursor = db.rawQuery(query, null)
        val inventoryPartsList = arrayListOf<InventoryPartDao>()
        while (cursor.moveToNext()) {
            inventoryPartsList.add(
                InventoryPartDao(
                    cursor.getInt(0),
                    cursor.getInt(1),
                    cursor.getInt(2),
                    cursor.getInt(3),
                    cursor.getInt(4),
                    cursor.getInt(5),
                    cursor.getInt(6),
                    cursor.getInt(7)
                )
            )
        }
        cursor.close()
        db.close()
        return inventoryPartsList
    }

    fun findAllInventories(): ArrayList<InventoryDao> {
        val query = "SELECT * FROM $TABLE_INVENTORIES ORDER BY LastAccessed DESC"

        val db = this.readableDatabase
        val cursor = db.rawQuery(query, null)
        val inventoryList = arrayListOf<InventoryDao>()
        while (cursor.moveToNext()) {
            inventoryList.add(InventoryDao(cursor.getInt(0), cursor.getString(1), cursor.getInt(2), cursor.getInt(3)))
        }
        cursor.close()
        db.close()
        return inventoryList
    }
    fun findAllActiveInventories(): ArrayList<InventoryDao> {
        val query = "SELECT * FROM $TABLE_INVENTORIES WHERE Active=1 ORDER BY LastAccessed DESC"

        val db = this.readableDatabase
        val cursor = db.rawQuery(query, null)
        val inventoryList = arrayListOf<InventoryDao>()
        while (cursor.moveToNext()) {
            inventoryList.add(InventoryDao(cursor.getInt(0), cursor.getString(1), cursor.getInt(2), cursor.getInt(3)))
        }
        cursor.close()
        db.close()
        return inventoryList
    }



    fun findCodeToImage(colorID: Int, itemID: Int): Int {

        var query2 = "SELECT * FROM Codes WHERE ColorID=$colorID and ItemID=$itemID"
        val db = this.readableDatabase
        var codeToImage = 0
        var cursor2 = db.rawQuery(query2, null)
        if (cursor2.moveToNext()) {
            codeToImage = cursor2.getInt(3)

        }
        cursor2.close()

        db.close()
        return codeToImage
    }

    fun createPartDto(inventoryPartDao: InventoryPartDao): PartDto {
        var partDto = PartDto()
        partDto.id = inventoryPartDao.id
        partDto.quantityInSet = inventoryPartDao.quantityInSet
        partDto.quantityInStore = inventoryPartDao.quantityInStore
        var query = "SELECT * FROM Parts WHERE id=\"${inventoryPartDao.itemID}\""
        val db = this.readableDatabase
        var cursor = db.rawQuery(query, null)
        if (cursor.moveToNext()) {
            partDto.namePart = cursor.getString(3)
            Log.e("*****partDto.namePart=", partDto.namePart + " dla itemID:" + inventoryPartDao.itemID)
        }
        Log.e("##### ", " =" + inventoryPartDao.itemID + "+++++" + partDto.namePart)
        cursor.close()

        var query1 = "SELECT * FROM Colors WHERE id=\"${inventoryPartDao.colorID}\""
        var cursor1 = db.rawQuery(query1, null)
        if (cursor1.moveToNext()) {
            partDto.nameColor = cursor1.getString(2)
            Log.e("*****partDto.colorName=", partDto.nameColor + " dla colorID" + inventoryPartDao.colorID)
        }
        cursor1.close()

        var query2 =
            "SELECT * FROM Codes WHERE ColorID=${inventoryPartDao.colorID} and ItemID=${inventoryPartDao.itemID}"
        var cursor2 = db.rawQuery(query2, null)
        if (cursor2.moveToNext()) {
            partDto.codeToImage = cursor2.getInt(3)
            Log.e(
                "Code dla colorID:",
                "" + inventoryPartDao.colorID + " itemID=" + inventoryPartDao.itemID + "to: " + partDto.codeToImage
            )
        }

//        partDto.image = findImage(partDto.codeToImage)
        partDto.image = findImage(inventoryPartDao.colorID,inventoryPartDao.itemID)
        cursor2.close()

        db.close()
        return partDto
    }


    /**
     * Creates a empty database on the system and rewrites it with your own database.
     */
    @Throws(IOException::class)
    fun createDataBase() {

        val dbExist = checkDataBase()

        if (dbExist) {
            //do nothing - database already exist
        } else {

            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            this.readableDatabase

            try {

                copyDataBase()

            } catch (e: IOException) {

                throw Error("Error copying database")

            }

        }

    }

    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    private fun checkDataBase(): Boolean {

        var checkDB: SQLiteDatabase? = null

        try {
            val myPath = DB_PATH + DATABASE_NAME
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY)

        } catch (e: SQLiteException) {

            //database does't exist yet.

        }

        if (checkDB != null) {

            checkDB.close()

        }

        return if (checkDB != null) true else false
    }


    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     */
    @Throws(IOException::class)
    private fun copyDataBase() {

        //Open your local db as the input stream
        val myInput = myContext.getAssets().open(DATABASE_NAME)

        // Path to the just created empty db
        val outFileName = DB_PATH + DATABASE_NAME

        //Open the empty db as the output stream
        val myOutput = FileOutputStream(outFileName)

        //transfer bytes from the inputfile to the outputfile
        val buffer = ByteArray(1024)
        var length: Int = myInput.read(buffer)
        while (length > 0) {
            myOutput.write(buffer, 0, length)
            length = myInput.read(buffer)
        }

        //Close the streams
        myOutput.flush()
        myOutput.close()
        myInput.close()

    }

    //
    fun selectAll(productName: String): Boolean {
        var result = false
        val query = "SELECT * FROM $TABLE_INVENTORIES_PARTS"

        val db = this.writableDatabase
        val cursor = db.rawQuery(query, null)
        cursor.moveToNext()
        while (!cursor.isAfterLast) {
//            val id = Integer.parseInt(cursor.getString(0))
//            db.delete(TABLE_PRODUCTS, COLUMN_ID + " = ?",arrayOf(id.toString()) )
            Log.e(
                "PARTS :", cursor.getString(0) + " : " + cursor.getString(1) + " : " + cursor.getString(2) +
                        " : " + cursor.getString(3) + " : " + cursor.getString(4) + " : " + cursor.getString(5)
                        + " : " + cursor.getString(6) + " : " + cursor.getString(7)
            )
            result = true
            cursor.moveToNext()
        }
        Log.e("kkkk", "l")
        cursor.close()
        db.close()
        return result
    }
}