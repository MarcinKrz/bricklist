package pl.put.poznan.krzeminski_marcin.bricklist

import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_export.*
import kotlinx.android.synthetic.main.content_export.*
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import java.io.File
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.OutputKeys
import javax.xml.transform.Transformer
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult

class ExportActivity : AppCompatActivity() {

    var id: Int = 0
    var conditionOpt: String = "No"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_export)
        setSupportActionBar(toolbar)
        id = (intent.extras ?: return).getInt("id")
        conditionsRadioGroup.check(R.id.newOption)
        conditionsRadioGroup.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.newOption -> conditionOpt = "N"
                R.id.usedOption -> conditionOpt = "U"
                R.id.noMatterOption -> conditionOpt = "No"
            }
        }
        exportButton.setOnClickListener {
            Log.e("!!!!!", "!!!!!kl")
            exportToFile()
//            read()
        }

        title = "Export"
    }

    fun exportToFile() {
        val dbHandler = Database(this, null, null, 1)
        val allItems = dbHandler.findAllInventoryParts(id)
        var elementsToXml = ArrayList<ItemToXml>()
        for (item in allItems) {
            var elem = ItemToXml()
            elem.itemtype = dbHandler.findCodeFromItemType(item.typeID)
            elem.itemid = dbHandler.findCodeFromParts(item.itemID)
            elem.color = dbHandler.findColorCodeFromColors(item.colorID)
            elem.qtyfilled = item.quantityInSet - item.quantityInStore
            elementsToXml.add(elem)
        }
        val docBuilder: DocumentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
        val doc: Document = docBuilder.newDocument()

        val rootElement: Element = doc.createElement("INVENTORY")
        for (elem in elementsToXml) {
            val item: Element = doc.createElement("ITEM")

            val itemType: Element = doc.createElement("ITEMTYPE")
            itemType.appendChild(doc.createTextNode(elem.itemtype))
            item.appendChild(itemType)

            val itemId: Element = doc.createElement("ITEMID")
            itemId.appendChild(doc.createTextNode(elem.itemid))
            item.appendChild(itemId)

            val color: Element = doc.createElement("COLOR")
            color.appendChild(doc.createTextNode(elem.color.toString()))
            item.appendChild(color)

            val qtyfilled: Element = doc.createElement("QTYFILLED")
            qtyfilled.appendChild(doc.createTextNode(elem.qtyfilled.toString()))
            item.appendChild(qtyfilled)

            if(!conditionOpt.equals("No")) {
                val condition: Element = doc.createElement("CONDITION")
                condition.appendChild(doc.createTextNode(conditionOpt))
                item.appendChild(condition)
            }

            rootElement.appendChild(item)
        }
        doc.appendChild(rootElement)
        val transformer: Transformer = TransformerFactory.newInstance().newTransformer()

        transformer.setOutputProperty(OutputKeys.INDENT, "yes")
        transformer.setOutputProperty("{http://xml/apache.org/xslt}ident-amount", "2")

//        val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            val path = this.getExternalFilesDir(
                Environment.DIRECTORY_DOWNLOADS)
//        val path = this.filesDir
        val outDir = File(path, "Output")
        outDir.mkdir()
        val file = File(outDir, "text.xml")

        transformer.transform(DOMSource(doc), StreamResult(file))
        Toast.makeText(this,"Dane zostały pomyślnie wyeksportowane do pliku text.xml",Toast.LENGTH_SHORT).show()

    }


    fun read() {
        var itemsList: MutableList<ItemContainer> = mutableListOf()
        val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
//        val path = this.filesDir
        val outDir = File(path, "Output")
        outDir.mkdir()
        val file = File(outDir, "text.xml")
        if (file.exists()) {
            val xmlDoc: Document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file)
            xmlDoc.documentElement.normalize()

            val items: NodeList = xmlDoc.getElementsByTagName("ITEM")
            for (i in 0..items.length - 1) {
                val itemNode: Node = items.item(i)
                if (itemNode.getNodeType() == Node.ELEMENT_NODE) {
                    val elem = itemNode as Element
                    val children = elem.childNodes
                    for (j in 0..children.length - 1) {
                        val node = children.item(j)
                        if (node is Element) {
                            Log.e("czytam: ", node.nodeName + ": " + node.textContent)
                        }
                    }


                }
            }

        }
    }

    inner class ItemToXml {
        var itemtype: String = ""
        var itemid: String = ""
        var color: Int = 0
        var qtyfilled: Int = 0
        var condition: String = ""
    }
}
