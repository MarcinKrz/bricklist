package pl.put.poznan.krzeminski_marcin.bricklist

import android.app.Application

class MyApplication : Application() {
    companion object GlobalVariables {
        var prefixUrl = "http://fcds.cs.put.poznan.pl/MyWeb/BL/"
        var postfixUrl = ".xml"
    }
}