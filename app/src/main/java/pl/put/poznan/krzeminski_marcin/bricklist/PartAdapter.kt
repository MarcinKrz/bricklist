package pl.put.poznan.krzeminski_marcin.bricklist

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

class PartAdapter : BaseAdapter {

    var partsList = ArrayList<PartDto>()
    var context: Context? = null

    constructor(partsList: ArrayList<PartDto>, context: Context?) : super() {
        this.partsList = partsList
        this.context = context
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val view: View?
        val viewHolder: ViewHolder

        if (convertView == null) {
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(R.layout.part_row, parent, false)
            viewHolder = ViewHolder(view)
            view.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }

        viewHolder.namePartText.text = partsList[position].namePart
        viewHolder.nameColorText.text = partsList[position].nameColor

        if (partsList[position].image != null) {
            viewHolder.imageView.setImageBitmap(partsList[position].image)
        }
        else{
            viewHolder.imageView.setImageResource(R.drawable.emptyimg)
        }

        viewHolder.quantityText.text =
            partsList[position].quantityInStore.toString() + "/" + partsList[position].quantityInSet

        if (partsList[position].quantityInSet == partsList[position].quantityInStore) {
            view?.setBackgroundColor(Color.GREEN)
        } else {
            view?.setBackgroundColor(Color.WHITE)
        }


        viewHolder.addButton.setOnClickListener {
            val newQuantity = partsList[position].quantityInStore + 1

            if (partsList[position].quantityInSet >= newQuantity) {
                partsList[position].quantityInStore = newQuantity
                if (partsList[position].quantityInSet == partsList[position].quantityInStore) {
                    view?.setBackgroundColor(Color.GREEN)
                } else {
                    view?.setBackgroundColor(Color.WHITE)
                }
                val dbHandler = Database(context!!, null, null, 1)
                dbHandler.updateQuantity(partsList[position].id, newQuantity)
                dbHandler.close()
//                (context as InventoryPartsActivity).refreshPartsListView()
                viewHolder.quantityText.text =
                    partsList[position].quantityInStore.toString() + "/" + partsList[position].quantityInSet
            }
        }

        viewHolder.reduceButton.setOnClickListener {
            val newQuantity = partsList[position].quantityInStore - 1

            if (partsList[position].quantityInSet >= newQuantity && newQuantity >= 0) {
                partsList[position].quantityInStore = newQuantity
                if (partsList[position].quantityInSet == partsList[position].quantityInStore) {
                    view?.setBackgroundColor(Color.GREEN)
                } else {
                    view?.setBackgroundColor(Color.WHITE)
                }
                val dbHandler = Database(context!!, null, null, 1)
                dbHandler.updateQuantity(partsList[position].id, newQuantity)
                dbHandler.close()
                viewHolder.quantityText.text =
                    partsList[position].quantityInStore.toString() + "/" + partsList[position].quantityInSet
//                (context as InventoryPartsActivity).refreshPartsListView()
            }
        }
        var selectedItem = getItem(position)
        viewHolder.namePartText.setOnClickListener {
            Toast.makeText(context, selectedItem.nameColor, Toast.LENGTH_SHORT).show()
        }

        return view
    }


    private class ViewHolder(view: View?) {
        val namePartText: TextView
        val nameColorText: TextView
        val quantityText: TextView
        val addButton: Button
        val reduceButton: Button
        val imageView: ImageView


        init {
            this.namePartText = view?.findViewById(R.id.namePartTextView) as TextView
            this.nameColorText = view?.findViewById(R.id.nameColorTextView) as TextView
            this.quantityText = view?.findViewById(R.id.quantityText) as TextView
            this.addButton = view?.findViewById(R.id.addPartButton) as Button
            this.reduceButton = view?.findViewById(R.id.reducePartButton) as Button
            this.imageView = view?.findViewById(R.id.imageView) as ImageView
        }
    }


    override fun getItem(position: Int): PartDto {
        return partsList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return partsList.size
    }
}