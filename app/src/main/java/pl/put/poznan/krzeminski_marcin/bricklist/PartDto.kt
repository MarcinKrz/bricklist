package pl.put.poznan.krzeminski_marcin.bricklist

import android.graphics.Bitmap

class PartDto {
    var id: Int = 1
    var namePart: String = ""
    var nameColor: String = ""
    var quantityInSet: Int = 0
    var quantityInStore: Int = 0
    var codeToImage: Int = 0
    var image:Bitmap?=null
}